#pragma once

//
//  .Net ����� ������� � ����������� ������
//

namespace CppInterface
{
    public ref class IDebugger                                                      //.Net ����� ������� � ����������� ������
    {
    public:                                                                         //�������� ������

        IDebugger()  {}                                                             //����������� ������
        ~IDebugger() {}                                                             //���������� ������

        System::Int16 GetAddrReg1();                                                //������ �������� �������
        System::Int16 GetAddrReg2();                                                //������ �������� �������

        System::Int16 GetIP();                                                      //Instruction Point

        System::Int32  GetInt(System::Int16 address);                               //����� �������� �� ������
        System::Single GetFloat(System::Int16 address);                             //������� �������� �� ������

        array<System::String^>^ GetLoadLog();                                               //������ ����������� ������
    };
}