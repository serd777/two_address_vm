#include <Stdafx.h>
#include "Debugger.h"

#include "../../virtual_machine/debugger/debugger.h"

using namespace CppInterface;
using namespace System;

Int16 IDebugger::GetAddrReg1()                                                      //������ �������� �������
{
    return Debugger::getAddrReg1();                                                 //�������� �������� �����
}
Int16 IDebugger::GetAddrReg2()                                                      //������ �������� �������
{
    return Debugger::getAddrReg2();                                                 //�������� �������� �����
}

Int16 IDebugger::GetIP()                                                            //Instruction Point
{
    return Debugger::getIp();                                                       //�������� �������� �����
}

System::Int32 IDebugger::GetInt(System::Int16 address)                              //����� �������� �� ������
{
    return Debugger::getInt(address);                                               //�������� �������� �����
}
System::Single IDebugger::GetFloat(System::Int16 address)                           //������� �������� �� ������
{
    return Debugger::getFloat(address);                                             //�������� �������� �����
}

array<String^>^ CppInterface::IDebugger::GetLoadLog()
{
    auto lines = Debugger::logs();                                                  //������ � ������
    auto count = lines.size();                                                      //���-�� �����
    array<String^>^ result = gcnew array<String^>(count);                           //�������������� ������

    for (decltype(count) i = 0; i < count; i++)                                     //�������� � Net ������
        result[i] = gcnew String(lines[i].c_str());                                 //������ std ������

    return result;                                                                  //���������� ���������
}
