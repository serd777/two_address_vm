#pragma once

//
//  .Net ����� ������� � ����������� ������
//

#include "../../virtual_machine/vm.h"

namespace CppInterface
{
    public ref class IVirtualMachine                                                //.Net ����� ������� � ����������� ������
    {
    public:                                                                         //�������� ������

        IVirtualMachine();                                                          //����������� ������
        ~IVirtualMachine();                                                         //���������� ������

        void LoadFile(System::String^ path);                                        //����� �������� ����� � ����������� ������
        
        void Interpret();                                                           //����� ������� ������������� ������������ �����
        void InterpretStep();                                                       //����� ���������� ������ ���� ����������� ������

        void Reset();                                                               //����� ������ ��������� ������
        
        bool IsFinished();                                                          //����� �������� ���������

    private:                                                                        //�������� ������

        VirtualMachine* vm_;                                                        //����������� ������
    };
}