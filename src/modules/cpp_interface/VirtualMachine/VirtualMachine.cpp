#include <Stdafx.h>
#include "VirtualMachine.h"

#include <string>
#include <msclr/marshal_cppstd.h>

using namespace CppInterface;
using namespace msclr::interop;

IVirtualMachine::IVirtualMachine()                                                  //����������� ������
{
    vm_ = new VirtualMachine;                                                       //�������������� ����������� ������
}
IVirtualMachine::~IVirtualMachine()                                                 //���������� ������
{
    delete vm_;                                                                     //����������� ������
}

//
//  ������ ������
//

void IVirtualMachine::LoadFile(System::String^ path)                                //����� �������� ����� � ����������� ������
{
    vm_->loadFile(marshal_as<std::wstring>(path));                                  //�������� �������� �����
}

void IVirtualMachine::Interpret()                                                   //����� ������� ������������� ������������ �����
{
    vm_->interpret();                                                               //�������� �������� �����
}
void IVirtualMachine::InterpretStep()                                               //����� ���������� ������ ���� ����������� ������
{
    vm_->interpretStep();                                                           //�������� �������� �����
}

void IVirtualMachine::Reset()                                                       //����� ������ ��������� ������
{
    vm_->reset();                                                                   //�������� �������� �����
}

bool IVirtualMachine::IsFinished()                                                  //����� �������� ���������
{
    return vm_->isFinished();                                                       //�������� �������� �����
}