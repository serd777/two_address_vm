#pragma once

//
//  ����������� ������
//

#include "export.h"

#include <string>

class VM_EXPORT VirtualMachine                                                      //����������� ������
{
public:                                                                             //�������� ������

    VirtualMachine()  {}                                                            //����������� ������
    ~VirtualMachine() {}                                                            //���������� ������

    void loadFile(const std::wstring& path);                                        //����� �������� ����� � ����������� ������

    void interpret();                                                               //����� ������� �������������
    void interpretStep();                                                           //����� ���������� ������ ���� (������� - ��� ������)

    void reset();                                                                   //����� ������ ���������
    bool isFinished();                                                              //����� �������� ���������
};