#include "table.h"

#include "../list/jumps/jmp/jmp.h"
#include "../list/jumps/jbe/jbe.h"
#include "../list/jumps/jnbe/jnbe.h"

#include "../list/system/stop/stop.h"
#include "../list/system/ld_reg/ld_reg.h"
#include "../list/system/cmp/cmp.h"

#include "../list/operations/add/add.h"
#include "../list/operations/fadd/fadd.h"

#include "../list/operations/sub/sub.h"
#include "../list/operations/fsub/fsub.h"

#include "../list/operations/div/div.h"
#include "../list/operations/fdiv/fdiv.h"

#include "../list/operations/mul/mul.h"
#include "../list/operations/fmul/fmul.h"

#include "../list/jumps/jz/jz.h"
#include "../list/jumps/jnz/jnz.h"

using namespace vm;

CommandTable::CommandTable()                                                        //����������� ������
{
    short_cmds_ = {                                                                 //������ 
        { OpCodes::JMP,  new commands::JMP(0)  },                                   //����������� �������
        { OpCodes::JNBE, new commands::JNBE(0) },                                   //JNBE
        { OpCodes::JBE,  new commands::JBE(0)  },                                   //JBE
        { OpCodes::JZ,   new commands::JZ(0)   },                                   //JZ
        { OpCodes::JNZ,  new commands::JNZ(0)  },                                   //JNZ
    };

    long_cmds_ = {
        { OpCodes::STOP,   new commands::STOP(0) },                                 //������� ���������
        { OpCodes::LD_REG, new commands::LD_REG(0) },                               //������� ��������
        { OpCodes::ADD,    new commands::ADD(0)  },                                 //������� �������������� ��������
        { OpCodes::FADD,   new commands::FADD(0) },                                 //������� ������������� ��������
        { OpCodes::SUB,    new commands::SUB(0)  },                                 //������� �������������� ���������
        { OpCodes::FSUB,   new commands::FSUB(0) },                                 //������� ������������� ���������
        { OpCodes::DIV,    new commands::DIV(0)  },                                 //������� �������������� �������
        { OpCodes::FDIV,   new commands::FDIV(0) },                                 //������� ������������� �������
        { OpCodes::MUL,    new commands::MUL(0)  },                                 //������� �������������� ���������
        { OpCodes::FMUL,   new commands::FMUL(0) },                                 //������� ������������� ���������
        { OpCodes::CMP,    new commands::CMP(0)  },                                 //������� ���������
    };
}

//
//  ������
//

bool CommandTable::isShort(const OpCodes& opcode)                                   //�����, ����������� �������� �� ������� - ��������
{
    return findShort(opcode);                                                       //���� ����� ���������, �� ��� �������� �������
}

Command* CommandTable::findCmd(                                                     //����� ������ �������� ������
    const OpCodes& opcode,                                                          //��� ��������
    const ShortCommand::Bits& bits                                                  //����� �����
)
{
    auto cmd = findShort(opcode);                                                   //���� �������
    if (cmd) cmd->setBits(bits);                                                    //���� ������� ���� �������, ������� ����

    return cmd;                                                                     //���������� �������
}
Command* CommandTable::findCmd(                                                     //����� ������ ������� ������
    const OpCodes& opcode,                                                          //��� ��������
    const LongCommand::Bits& bits                                                   //����� �����
)
{
    auto cmd = findLong(opcode);                                                    //���� �������
    if (cmd) cmd->setBits(bits);                                                    //���� ������� ���� �������, ������� ����

    return cmd;                                                                     //���������� �������
}

Command* CommandTable::findShort(const OpCodes& opcode)                             //����� ����� �������� ������
{
    Command* cmd = nullptr;                                                         //��������� �������
    auto count = short_cmds_.size();                                                //���-�� �������� ������

    for (decltype(count) i = 0; i < count; i++)                                     //���������� ���������
        if (short_cmds_[i].first == opcode)                                         //���� ����� ��� ��������
            cmd = short_cmds_[i].second;                                            //���������� ���������

    return cmd;                                                                     //���������� �������
}
Command* CommandTable::findLong(const OpCodes& opcode)                              //����� ����� ������� ������
{
    Command* cmd = nullptr;                                                         //��������� �������
    auto count = long_cmds_.size();                                                 //���-�� ������� ������

    for (decltype(count) i = 0; i < count; i++)                                     //���������� ���������
        if (long_cmds_[i].first == opcode)                                          //���� ����� ��� ��������
            cmd = long_cmds_[i].second;                                             //���������� ���������

    return cmd;                                                                     //���������� �������
}

OpCodes CommandTable::extractOpCode(const WordSize& word) const                     //����� ����������� �� ����� ��� ��������
{
    return static_cast<OpCodes>(word >> 10);                                        //���������� ��� ��������
}

DoubleWordSize CommandTable::unionWords(                                            //����� ������������ ��� ����� � �������
    const WordSize& first,                                                          //������
    const WordSize& second                                                          //������
)
{
    DoubleWordSize result = first;                                                  //���������
    result <<= 16;                                                                  //�������� ����� �� 16 ���
    result |= second;                                                               //������������� ������ �����
    return result;                                                                  //���������� ���������
}

WordSize CommandTable::parseShort(const std::string& bits)                          //��������� ����� � �����
{
    WordSize result = 0;                                                            //���������
    auto length = bits.length();                                                    //���-�� ���

    for (decltype(length) i = 0; i < length; i++)                                   //���������� ����
    {
        WordSize bit = bits[i] - '0';                                               //�������� ��� � ��������� �������������
        result <<= 1;                                                               //������� �� ���� ��� �����
        result |= bit;                                                              //������������� � �����
    }

    return result;                                                                  //���������� ���������
}
DoubleWordSize CommandTable::parseLong(const std::string& bits)                     //��������� ����� � ������� �����
{
    DoubleWordSize result = 0;                                                      //���������
    auto length = bits.length();                                                    //���-�� ���

    for (decltype(length) i = 0; i < length; i++)                                   //���������� ����
    {
        DoubleWordSize bit = bits[i] - '0';                                         //�������� ��� � ��������� �������������
        result <<= 1;                                                               //������� �� ���� ��� �����
        result |= bit;                                                              //������������� � �����
    }

    return result;                                                                  //���������� ���������
}
