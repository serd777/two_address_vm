#pragma once

//
//  ������� ������ ���� �������
//

#include "../command.h"
#include "../types/short/short.h"
#include "../types/long/long.h"
#include "../../types.h"

#include <vector>
#include <utility>
#include <string>

namespace vm
{
    class CommandTable                                                              //������� ������ ���� �������
    {
        typedef std::vector<std::pair<OpCodes, Command*>> vec_pairs;                //��� ������ - ��������� ��� ��� - �������

    public:                                                                         //�������� ������

        static CommandTable& Instance()                                             //����� �������
        {
            static CommandTable table;                                              //������� ��������� ������ ���� ��� �� ����
            return table;                                                           //���������� ���
        }

        bool isShort(const OpCodes& opcode);                                        //�����, ����������� �������� �� ������� - ��������

        Command* findCmd(const OpCodes& opcode, const ShortCommand::Bits& bits);    //����� ������ �������� ������
        Command* findCmd(const OpCodes& opcode, const LongCommand::Bits& bits);     //����� ������ ������� ������

        OpCodes        extractOpCode(const WordSize& word) const;                   //����� ����������� �� ����� ��� ��������
        DoubleWordSize unionWords(const WordSize& first, const WordSize& second);   //����� ������������ ��� ����� � �������
        
        static WordSize       parseShort(const std::string& bits);                  //��������� ����� � �����
        static DoubleWordSize parseLong(const std::string& bits);                   //��������� ����� � ������� �����

    private:                                                                        //�������� ������

        CommandTable();                                                             //����������� ������
        ~CommandTable() {}                                                          //���������� ������

        CommandTable(CommandTable const&)             = delete;                     //��������� ����������� �����������
        CommandTable& operator= (CommandTable const&) = delete;                     //� �������� ������������

        vec_pairs short_cmds_;                                                      //������ �������� ������
        vec_pairs long_cmds_;                                                       //������ ������� ������

        Command* findShort(const OpCodes& opcode);                                  //����� ����� �������� ������
        Command* findLong(const OpCodes& opcode);                                   //����� ����� ������� ������
    };
}