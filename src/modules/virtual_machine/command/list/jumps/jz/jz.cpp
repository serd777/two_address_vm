#include "jz.h"

#include "../../../../processor/processor.h"

using namespace vm::commands;

void JZ::operator()()                                                               //���� �������
{
    auto& proc = Processor::Instance();                                             //�������� ������ � ����������

    if (proc.flags()[ProcessorFlags::ZF])                                           //���� ������� �����������
        JMP::operator()();                                                          //������ �������
    else
        proc.ip() += 1;                                                             //����� ��������� � ��������� �������
}
