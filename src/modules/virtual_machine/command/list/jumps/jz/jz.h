#pragma once

//
//  Jump if zero
//

#include "../jmp/jmp.h"

namespace vm
{
    namespace commands
    {
        class JZ : public JMP                                                       //Jump if zero
        {
        public:                                                                     //�������� ������

            explicit JZ(                                                            //����������� ������
                const Bits& bits                                                    //����
            ) : JMP(bits) {}                                                        //�������������� ������������ �����

            ~JZ() {}                                                                //���������� ������

            void operator()() override;                                             //�������� ����������
        };
    }
}