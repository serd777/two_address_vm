#include "jmp.h"
#include "../../../../processor/processor.h"

using namespace vm::commands;

void JMP::operator()()                                                              //���� �������
{
    auto& proc = Processor::Instance();                                             //�������� ������ � ����������
    AddressSize address = 0;                                                        //����� ������� ��������� � IP

    switch(flag_)                                                                   //��������� bb ����
    {
    case BBFlag::_00: address = proc.ip()    + offset_; break;                      //00 = IP + Offset
    case BBFlag::_01: address = proc.reg_1() + offset_; break;                      //01 = Reg_1 + Offset
    case BBFlag::_10: address = proc.reg_2() + offset_; break;                      //02 = Reg_2 + Offset
    case BBFlag::_11: break;                                                        //���������������
    }

    proc.ip() = address;                                                            //������ � IP �����
}
