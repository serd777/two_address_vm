#pragma once

//
//  ������� ������������ ��������
//

#include "../../../types/short/short.h"

namespace vm
{
    namespace commands
    {
        class JMP : public ShortCommand                                             //������� ������������ ��������
        {
        public:                                                                     //�������� ������

            explicit JMP(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : ShortCommand(bits) {}                                               //�������������� ������������ �����

            ~JMP() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ����������
        };
    }
}