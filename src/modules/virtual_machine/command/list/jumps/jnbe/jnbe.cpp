#include "jnbe.h"

#include "../../../../processor/processor.h"

using namespace vm::commands;

void JNBE::operator()()                                                             //���� �������
{
    auto& proc = Processor::Instance();                                             //�������� ������ � ����������

    if (!proc.flags()[ProcessorFlags::CF] || !proc.flags()[ProcessorFlags::ZF])     //���� ������� �����������
        JMP::operator()();                                                          //������ �������
    else
        proc.ip() += 1;                                                             //����� ��������� � ��������� �������
}
