#pragma once

//
//  Jump not bigger or equal
//

#include "../jmp/jmp.h"

namespace vm
{
    namespace commands
    {
        class JNBE : public JMP                                                     //Jump not bigger or equal
        {
        public:                                                                     //�������� ������

            explicit JNBE(                                                          //����������� ������
                const Bits& bits                                                    //����
            ) : JMP(bits) {}                                                        //�������������� ������������ �����

            ~JNBE() {}                                                              //���������� ������

            void operator()() override;                                             //�������� ����������
        };
    }
}