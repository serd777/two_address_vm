#pragma once

//
//  Jump bigger or equal
//

#include "../jmp/jmp.h"

namespace vm
{
    namespace commands
    {
        class JBE : public JMP                                                      //Jump bigger or equal
        {
        public:                                                                     //�������� ������

            explicit JBE(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : JMP(bits) {}                                                        //�������������� ������������ �����

            ~JBE() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ����������
        };
    }
}