#pragma once

//
//  Jump not zero
//

#include "../jmp/jmp.h"

namespace vm
{
    namespace commands
    {
        class JNZ : public JMP                                                      //Jump not zero
        {
        public:                                                                     //�������� ������

            explicit JNZ(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : JMP(bits) {}                                                        //�������������� ������������ �����

            ~JNZ() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ����������
        };
    }
}