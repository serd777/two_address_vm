#pragma once

//
//  ������� ���������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class SUB : public LongCommand                                              //������� ���������
        {
        public:                                                                     //�������� ������

            explicit SUB(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~SUB() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}