#pragma once

//
//  ������� ��������� ������������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class FSUB : public LongCommand                                             //������� ��������� ������������
        {
        public:                                                                     //�������� ������

            explicit FSUB(                                                          //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~FSUB() {}                                                              //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}