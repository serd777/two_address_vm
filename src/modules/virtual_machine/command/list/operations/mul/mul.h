#pragma once

//
//  ������� ���������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class MUL : public LongCommand                                              //������� ���������
        {
        public:                                                                     //�������� ������

            explicit MUL(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~MUL() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}