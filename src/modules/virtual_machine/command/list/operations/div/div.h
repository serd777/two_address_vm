#pragma once

//
//  ������� ������� �������������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class DIV : public LongCommand                                              //������� ������� �������������
        {
        public:                                                                     //�������� ������

            explicit DIV(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~DIV() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}