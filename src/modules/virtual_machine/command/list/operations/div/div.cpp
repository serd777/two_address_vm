#include "div.h"

#include "../../../../processor/processor.h"
#include "../../../../memory/memory.h"

using namespace vm::commands;

void DIV::operator()()                                                              //���� �������
{
    auto& processor = Processor::Instance();                                        //������ � ����������
    auto& memory = Memory::Instance();                                              //������ � ������

    auto value1 = memory.getInt(processor.reg_1());                                 //�������� ������ ��������
    auto value2 = memory.getInt(processor.reg_2());                                 //�������� ������ ��������

    memory.setInt(value1 / value2, processor.reg_1());                              //�� ����� ������� ���������� ��������
    processor.ip() += 2;                                                            //��������� � ��������� �������
}
