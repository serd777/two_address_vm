#pragma once

//
//  ������� ��������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class ADD : public LongCommand                                              //������� ��������
        {
        public:                                                                     //�������� ������

            explicit ADD(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~ADD() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}