#pragma once

//
//  ������� ��������� ������������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class FMUL : public LongCommand                                             //������� ��������� ������������
        {
        public:                                                                     //�������� ������

            explicit FMUL(                                                          //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~FMUL() {}                                                              //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}