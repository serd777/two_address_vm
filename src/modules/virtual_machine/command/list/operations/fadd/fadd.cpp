#include "fadd.h"

#include "../../../../processor/processor.h"
#include "../../../../memory/memory.h"

using namespace vm::commands;

void FADD::operator()()                                                             //���� �������
{
    auto& processor = Processor::Instance();                                        //������ � ����������
    auto& memory = Memory::Instance();                                              //������ � ������

    auto value1 = memory.getFloat(processor.reg_1());                               //�������� ������ ��������
    auto value2 = memory.getFloat(processor.reg_2());                               //�������� ������ ��������

    memory.setFloat(value1 + value2, processor.reg_1());                            //�� ����� ������� ���������� ��������
    processor.ip() += 2;                                                            //��������� � ��������� �������
}
