#pragma once

//
//  ������� �������� �������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class FADD : public LongCommand                                             //������� �������� �������
        {
        public:                                                                     //�������� ������

            explicit FADD(                                                          //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~FADD() {}                                                              //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}