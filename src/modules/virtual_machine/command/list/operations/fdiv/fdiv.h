#pragma once

//
//  ������� ������� ������������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class FDIV : public LongCommand                                             //������� ������� ������������
        {
        public:                                                                     //�������� ������

            explicit FDIV(                                                          //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~FDIV() {}                                                              //���������� ������

            void operator()() override;                                             //�������� ���������
        };
    }
}