#include "stop.h"
#include "../../../../processor/processor.h"

using namespace vm::commands;

void STOP::operator()()                                                             //���� �������
{
    auto& proc = Processor::Instance();                                             //�������� ������ � ����������
    proc.flags()[ProcessorFlags::EF] = 1;                                           //������ ���� ���������

    proc.ip() += 2;                                                                 //��������� � ��������� �������
}
