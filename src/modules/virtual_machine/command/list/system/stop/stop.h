#pragma once

//
//  ������� ���������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class STOP : public LongCommand                                             //������� ���������
        {
        public:                                                                     //�������� ������

            explicit STOP(                                                          //����������� ������
                const Bits& bits                                                    //����� �����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~STOP() {}                                                              //���������� ������

            void operator()() override;                                             //�������� ������ �������
        };
    }
}