#pragma once

//
//  ������� �������� �������� ���������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class LD_REG : public LongCommand                                           //������� �������� �������� ���������
        {
        public:                                                                     //�������� ������

            explicit LD_REG(                                                        //����������� ������
                const Bits& data                                                    //���� �������
            ) : LongCommand(data) {}                                                //�������������� ������������ �����

            ~LD_REG() {}                                                            //���������� ������

            void operator()() override;                                             //�������� ��������� �������
        };
    }
}