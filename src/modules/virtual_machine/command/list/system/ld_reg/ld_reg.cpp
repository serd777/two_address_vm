#include "ld_reg.h"

#include "../../../../processor/processor.h"

using namespace vm::commands;

void LD_REG::operator()()                                                           //���� �������
{
    auto& processor = Processor::Instance();                                        //������ � ����������

    switch(flag_)                                                                   //� ����������� �� �����, ��� ��������
    {
    case BBFlag::_00:
        {
            processor.reg_1() = addr_1_;                                            //������ ������� = ���������
            processor.reg_2() = addr_2_;                                            //������ ������� = ���������
        } break;
    case BBFlag::_01:
        {
            processor.reg_1() = addr_1_;                                            //������ ������� = ���������
            processor.reg_2() += addr_2_;                                           //������ ������� += �����
        } break;
    case BBFlag::_10:
        {
            processor.reg_1() += addr_1_;                                           //������ ������� += �����
            processor.reg_2() = addr_2_;                                            //������ ������� = ���������
        } break;
    case BBFlag::_11:
        {
            processor.reg_1() += addr_1_;                                           //������ ������� += �����
            processor.reg_2() += addr_2_;                                           //������ ������� += �����
        } break;
    }

    processor.ip() += 2;                                                            //��������� � ��������� �������
}
