#pragma once

//
//  ������� ���������
//

#include "../../../types/long/long.h"

namespace vm
{
    namespace commands
    {
        class CMP : public LongCommand                                              //������� ���������
        {
        public:                                                                     //�������� ������

            explicit CMP(                                                           //����������� ������
                const Bits& bits                                                    //����
            ) : LongCommand(bits) {}                                                //�������������� ������������ �����

            ~CMP() {}                                                               //���������� ������

            void operator()() override;                                             //�������� ������
        };
    }
}