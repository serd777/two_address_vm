#include "cmp.h"

#include "../../../../memory/memory.h"
#include "../../../../processor/processor.h"

using namespace vm::commands;

void CMP::operator()()                                                              //���� �������
{
    auto& memory = Memory::Instance();                                              //������ � ������
    auto& processor = Processor::Instance();                                        //������ � ����������

    auto value1 = memory.getInt(processor.reg_1());                                 //������ ��������
    auto value2 = memory.getInt(processor.reg_2());                                 //������ ��������

    auto result = value1 - value2;                                                  //��������� ���������

    if (result > value1)
        processor.flags()[ProcessorFlags::OF] = 1;                                  //���� ������������ ����� ���������, ���� OF = 1
    else
        processor.flags()[ProcessorFlags::OF] = 0;                                  //����� 0

    if (!result)
        processor.flags()[ProcessorFlags::ZF] = 1;                                  //�������� �����������, ���� ZF = 1
    else
        processor.flags()[ProcessorFlags::ZF] = 0;                                  //����� 0

    if ((std::numeric_limits<int>::max() - value1) < value2)
        processor.flags()[ProcessorFlags::CF] = 1;                                  //��� ����� ����� ������������, ���� CF = 1
    else
        processor.flags()[ProcessorFlags::CF] = 0;                                  //����� 0
}
