#include "long.h"

#include "../../table/table.h"

using namespace vm;

LongCommand::LongCommand(const Bits& bits)                                      //�������� ������������
{
    //�������� ������ �� ����� � ���� ������
    setBits(bits);
}

void LongCommand::setBits(const Bits& bits)                                     //�����, �������� ���� �������
{
    auto word = CommandTable::parseLong(bits.to_string());                      //�������� � �����

    loadOpcode(word);                                                           //��������� ��� ��������
    loadBBFlag(word);                                                           //��������� BB ����
    loadAddrFirst(word);                                                        //��������� ������ �����
    loadAddrSecond(word);                                                       //��������� ������ �����
}

void LongCommand::loadOpcode(const DoubleWordSize& big_word)                    //�����, ����������� ��� �������� �� ������ �����
{
    Byte opcode = big_word >> 26;                                               //��� �������� � �����
    opcode_ = static_cast<OpCodes>(opcode);                                     //��������� � ���������
}
void LongCommand::loadBBFlag(const DoubleWordSize& big_word)                    //�����, ����������� BB ���� �� ������ �����
{
    Byte bb_flag = big_word << 6 >> 30;                                         //BB ���� � �����
    flag_ = static_cast<BBFlag>(bb_flag);                                       //��������� � ���������
}
void LongCommand::loadAddrFirst(const DoubleWordSize& big_word)                 //�����, ����������� ������ ����� � ����
{
    addr_1_ = big_word << 8 >> 20;                                              //������ �����
}
void LongCommand::loadAddrSecond(const DoubleWordSize& big_word)                //�����, ����������� ������ ����� � ����
{
    addr_2_ = big_word << 20 >> 20;                                             //������ �����
}
