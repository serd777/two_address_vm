#pragma once

//
//  ������� ������� (�� ���� ����)
//

#include "../../../types.h"
#include "../../command.h"

#include <bitset>

namespace vm
{
    class LongCommand : public Command                                              //������� ������� (�� ���� ����)
    {
        static constexpr size_t BitsAmount = 32;                                    //���-�� ����� � �������

    public:                                                                         //�������� ������

        typedef std::bitset<BitsAmount> Bits;                                       //��� ������ - ����� ����� �� BitsAmount

        explicit LongCommand(                                                       //����������� ������
            const Bits& bits                                                        //���� �������
        );
        ~LongCommand() {}                                                           //���������� ������

        void setBits(const Bits& bits) override;                                    //�����, �������� ���� �������

    protected:                                                                      //����������� ������

        AddressSize addr_1_;                                                        //������ �����
        AddressSize addr_2_;                                                        //������ �����

    private:                                                                        //�������� ������

        void loadOpcode(const DoubleWordSize& big_word);                            //�����, ����������� ��� �������� �� ������ �����
        void loadBBFlag(const DoubleWordSize& big_word);                            //�����, ����������� BB ���� �� ������ �����
        void loadAddrFirst(const DoubleWordSize& big_word);                         //�����, ����������� ������ �����
        void loadAddrSecond(const DoubleWordSize& big_word);                        //�����, ����������� ������ �����
    };
}
