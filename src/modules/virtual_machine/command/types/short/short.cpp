#include "short.h"
#include "../../table/table.h"

using namespace vm;

ShortCommand::ShortCommand(const Bits& bits)                                    //�������� ������������
{
    //�������� ������ �� ����� � ���� ������
    setBits(bits);
}

void ShortCommand::setBits(const Bits& bits)                                    //�����, �������� ���� �������
{
    auto word = CommandTable::parseShort(bits.to_string());                     //�����

    loadOpcode(word);                                                           //��������� ��� ��������
    loadBBFlag(word);                                                           //��������� BB ����
    loadOffset(word);                                                           //��������� ��������
}

void ShortCommand::loadOpcode(const WordSize& word)                             //�����, ����������� ��� �������� �� ������ �����
{
    Byte opcode = word >> 10;                                                   //��� �������� � �����
    opcode_ = static_cast<OpCodes>(opcode);                                     //��������� � ���������
}
void ShortCommand::loadBBFlag(const WordSize& word)                             //�����, ����������� BB ���� �� ������ �����
{
    Byte bb_flag = word << 6 >> 14;                                             //BB ���� � �����
    flag_ = static_cast<BBFlag>(bb_flag);                                       //��������� � ���������
}
void ShortCommand::loadOffset(const WordSize& word)                             //�����, ����������� ��������
{
    offset_ = word << 8 >> 8;                                                   //��������
}
