#pragma once

//
//  �������� ������� (�� ������ �����)
//

#include "../../../types.h"
#include "../../command.h"

#include <bitset>

namespace vm
{
    class ShortCommand : public Command                                             //�������� ������� (�� ������ �����)
    {
        static constexpr size_t BitsAmount = 16;                                    //���-�� ����� � �������

    public:                                                                         //�������� ������

        typedef std::bitset<BitsAmount> Bits;                                       //��� ������ - ����� ����� �� BitsAmount

        explicit ShortCommand(                                                      //����������� ������
            const Bits& bits                                                        //���� �������
        );
        ~ShortCommand() {}                                                          //���������� ������

        void setBits(const Bits& bits) override;                                    //�����, �������� ���� �������

    protected: OffsetSize offset_;                                                  //��������

    private:                                                                        //�������� ������

        void loadOpcode(const WordSize& word);                                      //�����, ����������� ��� �������� �� ������ �����
        void loadBBFlag(const WordSize& word);                                      //�����, ����������� BB ���� �� ������ �����
        void loadOffset(const WordSize& word);                                      //�����, ����������� ��������
    };
}
