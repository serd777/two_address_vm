#pragma once

//
//  ����������� ������
//

#include <string>

namespace vm
{
    class Loader                                                                    //����������� ������
    {
    public:                                                                         //�������� ������

        static Loader& Instance()                                                   //����� �������
        {
            static Loader loader;                                                   //������� ����� ���� �� �� ��� ��� ������
            return loader;                                                          //���������� ������
        }

        void loadFile(const std::wstring& path);                                    //�����, ����������� ���� � ����������� ������

    private:                                                                        //�������� ������

        Loader()  {}                                                                //����������� ������
        ~Loader() {}                                                                //���������� ������

        Loader(Loader const&)             = delete;                                 //��������� ����������� �����������
        Loader& operator= (Loader const&) = delete;                                 //� �������� ������������

        void loadVariables(std::ifstream& stream);                                  //����� �������� ����������� ��������
        void loadInt(const std::string& value);                                     //����� �������� ������ �� �������� ������
        void loadFloat(const std::string& value);                                   //����� �������� �������� �� �������� ������

        void loadCommands(std::ifstream& stream);                                   //����� �������� ������
        void loadLongCmd(const std::string& cmd);                                   //����� �������� ������� �������
        void loadShortCmd(const std::string& cmd);                                  //����� �������� �������� �������

        void loadIP(std::ifstream& stream);                                         //����� �������� ��������� �������
    };
}