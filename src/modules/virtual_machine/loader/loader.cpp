#include "loader.h"

#include "../types.h"
#include "../processor/processor.h"
#include "../memory/memory.h"
#include "../command/table/table.h"
#include "../debugger/debugger.h"

#include <fstream>
#include <Windows.h>
#include <algorithm>

using namespace vm;

AddressSize tmp_addr = 0;                                                           //��������� ����� (������������ � ��������)

//
//  �������� �������
//

void Loader::loadFile(const std::wstring& path)                                     //�����, ����������� ���� � ����������� ������
{
    std::ifstream file(path);                                                       //����� ������

    if (file.is_open())                                                             //���� ������� ������
    {
        loadVariables(file);                                                        //��������� ���������
        loadCommands(file);                                                         //��������� �������
        loadIP(file);                                                               //��������� ��������� �������

        tmp_addr = 0;                                                               //���������� ��������� �����
    }
    else MessageBoxA(NULL, strerror(errno), "Error", MB_OK);                        //����� ������� ������
}

void Loader::loadVariables(std::ifstream& stream)                                   //����� �������� ����������� ��������
{
    int count;                                                                      //���-�� ��������
    std::string buf;                                                                //��������� ������

    getline(stream, buf);                                                           //��������� ������
    count = atoi(buf.c_str());                                                      //������������ � �����

    for(decltype(count) i = 0; i < count; i++)                                      //���������� ��� ��������
    {
        std::string type, value;                                                    //��� � ��������

        getline(stream, type);                                                      //��������� ���
        getline(stream, value);                                                     //��������� ��������

        if      (type == "INT")   loadInt(value);                                   //���� ����� ��������
        else if (type == "FLOAT") loadFloat(value);                                 //���� ������� ��������
    }
}

void Loader::loadInt(const std::string& value)                                      //����� �������� ������ �� �������� ������
{
    int i_val = atoi(value.c_str());                                                //�������� ������ � ������ ����
    Debugger::logInt(tmp_addr);                                                     //��������� � ��������
    
    auto& memory = Memory::Instance();                                              //�������� ������ � ������
    memory.setInt(i_val, tmp_addr);                                                 //������ ����� ��������

    tmp_addr += 2;                                                                  //��������� � ���������� ������
}
void Loader::loadFloat(const std::string& value)                                    //����� �������� �������� �� �������� ������
{
    float f_val = atof(value.c_str());                                              //�������� ������ � �������� ����
    Debugger::logFloat(tmp_addr);                                                   //��������� � ��������
    
    auto& memory = Memory::Instance();                                              //�������� ������ � ������
    memory.setFloat(f_val, tmp_addr);                                               //������ ����� ��������

    tmp_addr += 2;                                                                  //��������� � ���������� ������
}

void Loader::loadCommands(std::ifstream& stream)                                    //����� �������� ������
{
    int cmd_count;                                                                  //���-�� ������
    std::string buf;                                                                //��������� ������

    getline(stream, buf);                                                           //��������� ������
    cmd_count = atoi(buf.c_str());                                                  //������������ � �����

    for (decltype(cmd_count) i = 0; i < cmd_count; i++)                             //��������� ������ �������
    {
        std::string line;                                                           //������� � ���� �����
        getline(stream, line);                                                      //�������� ������

        Debugger::logCmd(tmp_addr, line);                                           //��������� �������
        line.erase(remove_if(line.begin(), line.end(), isspace), line.end());       //������� �������

        if (line.length() == 32)                                                    //���� ��� ������� �������
            loadLongCmd(line);                                                      //��������� ��� ������� �������
        else
            loadShortCmd(line);                                                     //����� ��� ��������
    }
}
void Loader::loadLongCmd(const std::string& cmd)                                    //����� �������� ������� �������
{
    loadShortCmd(cmd.substr(0, 16));                                                //������ ��������
    loadShortCmd(cmd.substr(16, 16));                                               //������ ��������
}
void Loader::loadShortCmd(const std::string& cmd)                                   //����� �������� �������� �������
{
    auto& table = CommandTable::Instance();                                         //������� ������
    auto word = table.parseShort(cmd);                                              //�������� �����

    auto& memory = Memory::Instance();                                              //�������� ������ � ������
    memory[tmp_addr++] = word;                                                      //������ ����� � ������
}

void Loader::loadIP(std::ifstream& stream)                                          //����� �������� ��������� �������
{
    std::string buf;                                                                //������
    getline(stream, buf);                                                           //����� ������� ������

    auto& processor = Processor::Instance();                                        //������ � ����������
    processor.ip() = atoi(buf.c_str());                                             //������������ � ����� � ������ � IP
}
