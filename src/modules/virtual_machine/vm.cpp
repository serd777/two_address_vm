#include "vm.h"

#include "loader/loader.h"
#include "processor/processor.h"
#include "command/table/table.h"
#include "memory/memory.h"
#include "debugger/debugger.h"

using namespace vm;

//
//  �������� �������
//

void VirtualMachine::loadFile(const std::wstring& path)                             //����� �������� ����� � ����������� ������
{
    auto& loader = Loader::Instance();                                              //�������� ������ � ����������
    loader.loadFile(path);                                                          //��������� ���� � ������
}

void VirtualMachine::interpret()                                                    //����� ������� �������������
{
    auto& processor = Processor::Instance();                                        //�������� ������ � ����������

    while (!processor.flags()[ProcessorFlags::EF])                                  //���� ���� ��������� �� ������
        interpretStep();                                                            //��������� ���������������� ������������
}
void VirtualMachine::interpretStep()                                                //����� ���������� ������ ���� (������� - ��� ������)
{
    auto& cmd_table = CommandTable::Instance();                                     //������ � ������� ������
    auto& processor = Processor::Instance();                                        //������ � ����������
    auto& memory    = Memory::Instance();                                           //������ � ������

    auto w_first = memory[processor.ip()];                                          //������� ������ ����� �������
    auto opcode = cmd_table.extractOpCode(w_first);                                 //������� ��� ��������

    Command* cmd = nullptr;                                                         //�������

    if (cmd_table.isShort(opcode))                                                  //���� ��� �������� �������
        cmd = cmd_table.findCmd(opcode, ShortCommand::Bits(w_first));               //���� �������
    else
    {
        auto w_second = memory[processor.ip() + 1];                                 //������� ������ �����
        auto w_double = cmd_table.unionWords(w_first, w_second);                    //���������� ��� �����

        cmd = cmd_table.findCmd(opcode, LongCommand::Bits(w_double));               //���� �������
    }

    if (cmd) (*cmd)();                                                              //�������� ��������� �������
}

void VirtualMachine::reset()                                                        //����� ������ ���������
{
    auto& proc = Processor::Instance();                                             //������ � ����������

    proc.ip()    = 0;                                                               //����� IP
    proc.reg_1() = 0;                                                               //����� ������� ��������
    proc.reg_2() = 0;                                                               //����� ������� ��������
    proc.flags().reset();                                                           //����� ������ ���������

    Debugger::clearLog();                                                           //������� ���
}

bool VirtualMachine::isFinished()                                                   //����� �������� ���������
{
    auto& processor = Processor::Instance();                                        //������ � ����������
    return processor.flags()[ProcessorFlags::EF];                                   //���� ������������ ���������
}
