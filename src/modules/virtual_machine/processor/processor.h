#pragma once

//
//  ��������� ����������� ������
//

#include "../types.h"

#include <bitset>

namespace vm
{
    enum ProcessorFlags                                                             //Enum ������ ����������
    {
        CF,                                                                         //Carry Flag
        PF,                                                                         //Parity Flag
        AF,                                                                         //Auxiliary Carry Flag
        ZF,                                                                         //Zero Flag
        SF,                                                                         //Sign Flag
        TF,                                                                         //Trap Flag
        IF,                                                                         //Interrupt Enable Flag
        DF,                                                                         //Direction Flag
        OF,                                                                         //Overflow Flag
        NT,                                                                         //Nested Task
        RF,                                                                         //Resume Flag
        EF,                                                                         //End Flag (���� ��������� ������)
    };

    class Processor                                                                 //��������� ����������� ������
    {
    public:                                                                         //�������� ������

        static Processor& Instance()                                                //����� �������
        {
            static Processor processor;                                             //������� ����� ���� �� �� ��� ��� ������
            return processor;                                                       //���������� ������
        }

        auto& ip()    { return ip_;    }                                            //������ � Instruction Point
        auto& flags() { return flags_; }                                            //������ � ������

        auto& reg_1() { return reg_1_; }                                            //������ � ������� ��������
        auto& reg_2() { return reg_2_; }                                            //������ �� ������� ��������

    private:                                                                        //�������� ������

        Processor()  {}                                                             //����������� ������
        ~Processor() {}                                                             //���������� ������

        Processor(Processor const&)             = delete;                           //��������� ����������� �����������
        Processor& operator= (Processor const&) = delete;                           //� �������� ������������

        AddressSize     reg_1_ = 0;                                                 //�������� ������� 1
        AddressSize     reg_2_ = 0;                                                 //�������� ������� 2

        AddressSize     ip_ = 0;                                                    //Instruction Point
        std::bitset<16> flags_;                                                     //����� ����������
    };
}