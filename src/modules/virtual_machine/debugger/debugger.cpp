#include "debugger.h"

#include "../processor/processor.h"
#include "../memory/memory.h"

using namespace vm;

Debugger::vec_st Debugger::logs_ = vec_st();                                        //�������������� ����������� ����������

AddressSize Debugger::getAddrReg1()                                                 //������ �������� �������
{
    auto& processor = Processor::Instance();                                        //�������� ������ � ����������
    return processor.reg_1();                                                       //���������� �������� ��������� ��������
}
AddressSize Debugger::getAddrReg2()                                                 //������ �������� �������
{
    auto& processor = Processor::Instance();                                        //�������� ������ � ����������
    return processor.reg_2();                                                       //���������� �������� ��������� ��������
}

IntSize Debugger::getInt(const AddressSize& address)                                //���������� ����� �������� �� ���������� ������
{
    auto& memory = Memory::Instance();                                              //�������� ������ � ������
    return memory.getInt(address);                                                  //���������� �������� �� ���������� ������
}
FloatSize Debugger::getFloat(const AddressSize& address)                            //���������� ������� �������� �� ���������� ������
{
    auto& memory = Memory::Instance();                                              //�������� ������ � ������
    return memory.getFloat(address);                                                //���������� �������� �� ���������� ������
}

AddressSize Debugger::getIp()                                                       //Instruction Point
{
    auto& processor = Processor::Instance();                                        //�������� ������ � ����������
    return processor.ip();                                                          //���������� �������� IP
}

void Debugger::logInt(const AddressSize& address)                                   //������ � ��� ������������ Int'�
{
    std::string line = std::to_string(address) + " - INT";                          //��������� ������
    logs_.push_back(line);                                                          //������ � ���
}
void Debugger::logFloat(const AddressSize& address)                                 //������ � ��� ������������ Float'�
{
    std::string line = std::to_string(address) + " - FLOAT";                        //��������� ������
    logs_.push_back(line);                                                          //������ � ���
}

void Debugger::logCmd(const AddressSize& address, const std::string& cmd)           //������ � ��� ����������� �������
{
    std::string line = std::to_string(address) + " - " + cmd;                       //��������� ������
    logs_.push_back(line);                                                          //������ � ���
}

void Debugger::clearLog()                                                           //����� ��������� ���
{
    logs_.clear();                                                                  //������� ���������
}
