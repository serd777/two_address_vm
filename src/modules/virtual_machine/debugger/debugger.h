#pragma once

//
//  ���������� ������
//

#include "../export.h"
#include "../types.h"

#include <vector>
#include <string>

class VM_EXPORT Debugger                                                            //���������� ������
{
    typedef std::vector<std::string> vec_st;                                        //��� ������ - ������ �����

public:                                                                             //�������� ������

    static AddressSize getAddrReg1();                                               //������ �������� �������
    static AddressSize getAddrReg2();                                               //������ �������� �������

    static IntSize   getInt(const AddressSize& address);                            //���������� ����� �������� �� ���������� ������
    static FloatSize getFloat(const AddressSize& address);                          //���������� ������� �������� �� ���������� ������

    static AddressSize getIp();                                                     //Instruction Point

    static void logInt(const AddressSize& address);                                 //������ � ��� ������������ Int'�
    static void logFloat(const AddressSize& address);                               //������ � ��� ������������ Float'�

    static void logCmd(const AddressSize& address, const std::string& cmd);         //������ � ��� ����������� �������

    static const vec_st& logs() { return logs_; }                                   //������ � �����
    static void clearLog();                                                         //�����, ��������� ���

private:                                                                            //�������� ������

    static vec_st logs_;                                                            //��� ��������
};