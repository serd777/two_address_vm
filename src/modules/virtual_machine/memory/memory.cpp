#include "memory.h"

#include <cassert>

using namespace vm;

Memory::Memory()                                                                    //����������� ������
{
    container_.resize(size_);                                                       //�������� ������
}

//
//  ���������
//

WordSize& Memory::operator[](const AddressSize& address)                            //�������� ������� � �����
{
    assert(address >= 0 && address < size_);                                        //������ �������
    return container_[address];                                                     //���������� ����� �� ������
}

//
//  ������ �������������� � �������
//

int Memory::getInt(const AddressSize& address)                                      //����� ������� � 4 ������ ������ �����
{
    assert(address >= 0 && address < size_);                                        //������ �������

    IntVal data;                                                                    //������������� �������������

    data.parts[0] = container_[address];                                            //������ ��� �����
    data.parts[1] = container_[address + 1];                                        //������ ��� �����

    return data.val;                                                                //���������� �����
}
float Memory::getFloat(const AddressSize& address)                                  //����� ������� � 4 ������ �������� �����
{
    assert(address >= 0 && address < size_);                                        //������ �������

    FloatVal data;                                                                  //������������ �������������

    ShortVal first;                                                                 //������ ��� �����
    ShortVal second;                                                                //������ ��� �����

    first.val = container_[address];                                                //������� ������ ��� �����
    second.val = container_[address + 1];                                           //������� ������ ��� �����

    data.parts[0] = first;                                                          //������ � ������ ������ ���
    data.parts[1] = second;                                                         //������ ���

    return data.val;                                                                //���������� �����
}

void Memory::setInt(int value, const AddressSize& address)                          //����� ������ ������ ����� �� ���������� ������
{
    assert(address >= 0 && address < size_);                                        //������ �������

    IntVal data;                                                                    //������������� �������������
    data.val = value;                                                               //������ ������������� ��������

    container_[address] = data.parts[0];                                            //������ ������ ��������
    container_[address + 1] = data.parts[1];                                        //������ ������ ��������
}
void Memory::setFloat(float value, const AddressSize& address)                      //����� ������ �������� ����� �� ���������� ������
{
    assert(address >= 0 && address < size_);                                        //������ �������

    FloatVal data;                                                                  //������������ �������������

    ShortVal first;                                                                 //������ ��� �����
    ShortVal second;                                                                //������ ��� �����

    data.val = value;                                                               //������ ��������

    first = data.parts[0];                                                          //��������� �� ������ �����
    second = data.parts[1];                                                         //��������� �� ������ �����

    container_[address] = first.val;                                                //������ � ��������� ������ �����
    container_[address + 1] = second.val;                                           //������ � ��������� ������ �����
}
