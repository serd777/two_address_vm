#pragma once

//
//  ������ ����������� ������
//

#include "../types.h"

#include <vector>

namespace vm
{
    class Memory                                                                    //������ ����������� ������
    {
        union IntVal                                                                //������������� ������������� � ���� ���� short
        {
            WordSize parts[2];                                                      //��� �����
            IntSize val;                                                            //� ����� �����
        };
        union ShortVal                                                              //������������� �����
        {
            Byte parts[2];                                                          //��� �����
            WordSize val;                                                           //� ����� �����
        };
        union FloatVal                                                              //������� ������������� � ���� 4�� ������
        {
            ShortVal parts[2];                                                      //��� �����
            FloatSize val;                                                          //� ����� ������������ �����
        };

    public:                                                                         //�������� ������
        
        static Memory& Instance()                                                   //����� �������
        {
            static Memory memory;                                                   //������� ������ ���� �� �� ��� ��� ������
            return memory;                                                          //���������� ���
        }

        WordSize& operator[](const AddressSize& address);                           //�������� �������

        int   getInt(const AddressSize& address);                                   //����� ������� � 4 ������ ������ �����
        float getFloat(const AddressSize& address);                                 //����� ������� � 4 ������ �������� �����

        void setInt(int value, const AddressSize& address);                         //����� ������ ������ ����� �� ���������� ������
        void setFloat(float value, const AddressSize& address);                     //����� ������ �������� ����� �� ���������� ������

    private:                                                                        //�������� ������

        Memory();                                                                   //����������� ������
        ~Memory() {}                                                                //���������� ������

        Memory(Memory const&)             = delete;                                 //��������� ����������� �����������
        Memory& operator= (Memory const&) = delete;                                 //� �������� ������������

        AddressSize size_ = 4096;                                                   //������ ����������
        std::vector<WordSize> container_;                                           //��������� ������
    };
}