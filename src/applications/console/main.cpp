//
//  �������� �������
//

#include "../../modules/virtual_machine/vm.h"
#include "../../modules/virtual_machine/debugger/debugger.h"

#include <iostream>

int main(int argc, char** argv)
{
    VirtualMachine vm;

    vm.loadFile(L"input.txt");

    std::cout << Debugger::getInt(0)   << std::endl;
    std::cout << Debugger::getInt(2) << std::endl;

    vm.interpret();

    std::cout << Debugger::getInt(0) << std::endl;
    std::cout << Debugger::getAddrReg1() << std::endl;
    std::cout << Debugger::getAddrReg2() << std::endl;

    system("pause");
    return 0;
}