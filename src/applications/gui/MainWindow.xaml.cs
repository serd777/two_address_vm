﻿using System.Globalization;
using System.Windows;
using CppInterface;
using Microsoft.Win32;

namespace gui
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Интерфейс виртуальной машины
        /// </summary>
        private readonly IVirtualMachine _iVirtualMachine;

        /// <summary>
        /// Интерфейс отладчика
        /// </summary>
        private readonly IDebugger _iDebugger;

        /// <summary>
        /// Конструктор
        /// </summary>
        public MainWindow()
        {
            //Инициализируем виртуальную машину
            _iVirtualMachine = new IVirtualMachine();
            //Инициализируем отладчик
            _iDebugger = new IDebugger();

            //Инициализируем компоненты
            InitializeComponent();
            //Отключаем контроллеры виртуальной машины
            DisableVmControls();
        }

        /// <summary>
        /// Метод обновления данных в полях
        /// </summary>
        private void UpdateData()
        {
            //Адресные регистры
            Addr1Box.Text = _iDebugger.GetAddrReg1().ToString();
            Addr2Box.Text = _iDebugger.GetAddrReg2().ToString();

            //IP
            IpBox.Text = _iDebugger.GetIP().ToString();
        }

        private void UpdateCommands()
        {
            //Очищаем контрол
            CommandList.Items.Clear();
            //Получаем лог загрузки
            var lines = _iDebugger.GetLoadLog();

            //Заносим в контрол
            foreach (var line in lines)
                CommandList.Items.Add(line);
        }

        /// <summary>
        /// Метод включающий контроллеры виртуальной машины
        /// </summary>
        private void EnableVmControls()
        {
            DebuggerBox.IsEnabled = true;
            StateBox.IsEnabled = true;
        }

        /// <summary>
        /// Метод, отключающий контроллеры виртуальной машины
        /// </summary>
        private void DisableVmControls()
        {
            DebuggerBox.IsEnabled = false;
            StateBox.IsEnabled = false;
        }

        /// <summary>
        /// Обработчик нажатия на кнопку открытия файла
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileOpenButton_Click(object sender, RoutedEventArgs e)
        {
            //Открываем диалоговое окно для выбора файла
            var dlg = new OpenFileDialog();

            //Если пользователь не выбрал файл
            if (dlg.ShowDialog(this) != true)
                return;

            //Заносим имя открытого файла в контроллер, если успешно выбран файл
            FilePathBox.Text = dlg.FileName;

            //Загружаем в виртуальной машину
            _iVirtualMachine.Reset();
            _iVirtualMachine.LoadFile(dlg.FileName);

            //Обновляем данные в полях и включаем контроллеры
            UpdateData();
            UpdateCommands();
            EnableVmControls();
        }

        /// <summary>
        /// Обработчик нажатия на кнопку запуска отладки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            //Запускаем интерпретацию
            _iVirtualMachine.Interpret();

            //Обновляем данные и отключаем контроллеры
            UpdateData();
            DisableVmControls();
        }

        /// <summary>
        /// Обработчик нажатия на кнопку шаг вперед в отладке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StepButton_Click(object sender, RoutedEventArgs e)
        {
            //Запускаем интерпретацию шага
            _iVirtualMachine.InterpretStep();

            //Обновляем данные
            UpdateData();
            //Отключаем контроллеры если программа завершилась
            if(_iVirtualMachine.IsFinished())
                DisableVmControls();
        }

        /// <summary>
        /// Обработчик кнопки остановки отладки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            //Сбрасываем состояние виртуальной машины
            _iVirtualMachine.Reset();

            //Отключаем контроллеры
            DisableVmControls();
        }

        /// <summary>
        /// Обработчик кнопки просмотра значения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void InspectValButton_Click(object sender, RoutedEventArgs e)
        {
            //Достаем адрес из контроллера
            var address = short.Parse(AddrValBox.Text);

            //Загружаем данные в контроллеры
            IntValBox.Text = _iDebugger.GetInt(address).ToString();
            FloatValBox.Text = _iDebugger.GetFloat(address).ToString(CultureInfo.CurrentCulture);
        }
    }
}
